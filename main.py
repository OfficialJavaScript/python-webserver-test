from flask import Flask, render_template
import socket
hostname = socket.gethostname()
IPAddr = socket.gethostbyname(hostname)
app = Flask(__name__)

@app.route('/')
def main():
    return render_template('index.html', ipaddress=IPAddr, servname=hostname)

if __name__ == ('__main__'):
    app.run(host='0.0.0.0', port=80)